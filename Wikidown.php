<?php
require_once(dirname(__FILE__).'/php-markdown/Michelf/Markdown.php');
require_once(dirname(__FILE__).'/php-markdown/Michelf/MarkdownExtra.php');

if (!defined('MEDIAWIKI')) {
  die('This is not a valid entry point to MediaWiki.');
}

$wgExtensionCredits['parserhook'][] = array(
  'name'        => 'Wikidown',
  'author'      => 'Xiao Jia',
  'version'     => '1.3',
  'url'         => 'https://github.com/stfairy/Wikidown',
  'description' => 'Markdown support for MediaWiki via &lt;wikidown&gt; tag',
);

$wgHooks['ParserFirstCallInit'][] = 'wfWikidownParserInit';
 
function wfWikidownParserInit(Parser $parser) {
  $parser->setHook('wikidown', 'wfWikidownRender');
  return true;
}

function wfWikidownRender($input, array $args, Parser $parser, PPFrame $frame) {
  return \Michelf\MarkdownExtra::defaultTransform($input);
}
